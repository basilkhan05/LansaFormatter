# README #

### LANSA Sublime Text 3 Formatter ###

* LANSA rdml/rdmlx syntax highlighting and code formatting support for Sublime Text 3
* Version 1.0.0

### How do I get set up? ###

* Clone repo to: <data_path>/Packages/LANSAFormatter
* Restart Sublime
* Any file with extensions .lansa, .rdml, .rdmlx will use LANSA syntax highlighting
* Or, use the sublime menu to set syntax to LANSA
* To format LANSA code, shortcut is Ctrl+Alt+x
* Or, use the sublime menu to look for LANSAFormatter:Format Code

### Features ###

* Sublime command pallet integration (Ctrl+Shift+p)
![menu.PNG](https://bitbucket.org/repo/xxaqj5/images/4254955372-menu.PNG)
* Syntax highlighting and formatting
![syntax-and-format.png](https://bitbucket.org/repo/xxaqj5/images/1670993633-syntax-and-format.png)
* Code completions and snippets
![completions.gif](https://bitbucket.org/repo/xxaqj5/images/2670903469-completions.gif)

### Who do I talk to? ###

* Feel free to submit an issue on Bitbucket
* For all other inquiries, please contact Marco Kam at marco.kam@gmail.com